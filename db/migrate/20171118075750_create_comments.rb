class CreateComments < ActiveRecord::Migration[5.1]
  def change
    create_table :comments do |t|
      t.string :message, null: false
      t.integer :group_id, null: false, index: true
      t.integer :user_id, null: :false, index: true
      t.integer :post_id, null: false, index: true
      t.boolean :is_deleted, default: false

      t.timestamps
    end
  end
end
