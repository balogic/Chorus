class CreateRoles < ActiveRecord::Migration[5.1]
  def change
    create_table :roles do |t|
      t.string :title, null: false
      t.boolean :can_approve, default: false, null: false
      t.boolean :can_add_admin, default: false, null: false
      t.boolean :can_delete, default: false, null: false
      t.boolean :can_activate, default: false, null: false

      t.timestamps
    end
  end
end
