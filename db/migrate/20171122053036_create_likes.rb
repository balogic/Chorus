class CreateLikes < ActiveRecord::Migration[5.1]
  def change
    create_table :likes do |t|
      t.integer :post_id, null:  false, index: true
      t.integer :group_id, null:  false, index: true
      t.integer :user_id, null:  false, index: true
      t.boolean :is_active, default: false

      t.timestamps
    end
  end
end
