class CreateInvites < ActiveRecord::Migration[5.1]
  def change
    create_table :invites do |t|
      t.integer :invitee_id, index: true, null: false, foreign_key: true
      t.integer :group_id, index: true, null: false
      t.integer :inviter_id, index: true, null: false, foreign_key: true
      t.integer :status, null: false

      t.timestamps
    end
  end
end
