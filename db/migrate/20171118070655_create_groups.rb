class CreateGroups < ActiveRecord::Migration[5.1]
  def change
    create_table :groups do |t|
      t.string :title, null: false
      t.text :description
      t.boolean :is_active, default: true, null: false
      t.boolean :is_deleted, default: false, null: false

      t.timestamps
    end
  end
end
