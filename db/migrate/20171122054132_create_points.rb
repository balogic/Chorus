class CreatePoints < ActiveRecord::Migration[5.1]
  def change
    create_table :points do |t|
      t.integer :category, null: false
      t.integer :value, null: false
      t.integer :user_id, null: false, index: true
      t.integer :group_id, null: false, index: true

      t.timestamps
    end
  end
end
