class CreatePosts < ActiveRecord::Migration[5.1]
  def change
    create_table :posts do |t|
      t.string :title, null: false
      t.text :message
      t.integer :user_id, null: false, index: true
      t.integer :group_id, null: false, index: true
      t.integer :status, null: false
      t.boolean :is_approved, default: false, null: false

      t.timestamps
    end
  end
end
