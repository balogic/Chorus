class Invite < ApplicationRecord
  enum status: { pending: 0, approved: 1, rejected: 2 }
  belongs_to :inviter, class_name: 'User'
  after_create :add_points
  after_update :check_approval

  def add_points
    Point.create(category: :invites, group_id: group_id, user_id: inviter_id, value: 2)
  end

  def check_approval
    if approved?
      Point.create(category: :invites, group_id: group_id, user_id: inviter_id, value: 1)
      Point.create(category: :invites, group_id: group_id, user_id: invitee_id, value: 1)
    end
  end
end
