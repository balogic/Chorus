class Point < ApplicationRecord
  enum category: { likes: 0, comments: 1, invites: 2, creates: 3 }
  belongs_to :user
  belongs_to :group
end
