class Comment < ApplicationRecord
  belongs_to :group
  belongs_to :user
  belongs_to :post
  after_create :add_points

  def add_points
    Point.create(category: :comments, group_id: group_id, user_id: user_id, value: 1)
  end
end
