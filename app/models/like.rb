class Like < ApplicationRecord
  belongs_to :post
  belongs_to :user
  belongs_to :group
  after_create :add_points

  def add_points
    Point.create(category: :likes, group_id: group_id, user_id: user_id, value: 1)
  end
end
