class User < ApplicationRecord
  has_and_belongs_to_many :groups
  belongs_to :invite, class_name: 'Invite', foreign_key: :invitee_id
  has_many :invites, class_name: 'Invite', foreign_key: :inviter_id
  has_many :posts
  has_many :likes
  has_many :points
end
