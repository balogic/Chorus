class Post < ApplicationRecord
  enum status: { pending: 0, approved: 1, rejected: 2 }
  has_many :comments
  belongs_to :user
  belongs_to :group
  has_many :likes
end
