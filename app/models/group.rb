class Group < ApplicationRecord
  has_and_belongs_to_many :users
  has_many :invites
  has_many :posts
  has_many :likes
  has_many :points
  after_create :add_points

  def add_points
    Point.create(category: :creates, group_id: id, user_id: user_id, value: 5)
  end
end
