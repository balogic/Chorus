class Notification
  # Returns Total points for a group and user in a particular time period
  def get_points(user)
    points = user.points.where(created_at: Time.now..(Time.now - 1.week))
    points.sum('value')
  end

  # Fetches the top users for the week
  def leaderboard(group)
    user_points = []
    group.users.includes(:points).each do |user|
      user_points << {
        'user_id' => user.id,
        'email' => user.email,
        'points' => get_points(user)
      }
    end
    top_users = user_points.sort_by { |k| k['points'] }
    top_users.last(10).reverse
  end

  # Sends Email to Top Users Each Week
  def send_email
    Group.includes(:users).find_each do |group|
      leaderboard(group)
      # Code to Send Email to Users
    end
  end
end
